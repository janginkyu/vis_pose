#include <exception>


#include <visp3/core/vpConfig.h>
#ifdef VISP_HAVE_MODULE_SENSOR
#include <visp3/sensor/vpV4l2Grabber.h>
#include <visp3/sensor/vp1394CMUGrabber.h>
#include <visp3/sensor/vp1394TwoGrabber.h>
#include <visp3/sensor/vpFlyCaptureGrabber.h>
#include <visp3/sensor/vpRealSense2.h>
#endif
//! [Include]
#include <visp3/detection/vpDetectorAprilTag.h>
//! [Include]
#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/gui/vpDisplayOpenCV.h>
#include <visp3/gui/vpDisplayX.h>
#include <visp3/core/vpXmlParserCamera.h>

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2/LinearMath/Quaternion.h>

int main(int argc, char **argv) {

    ros::init(argc, argv, "vis_pose");
    ros::NodeHandle n;

    std::string par;

    int opt_device = 2;
    vpDetectorAprilTag::vpAprilTagFamily tagFamily = vpDetectorAprilTag::TAG_36h11;
    vpDetectorAprilTag::vpPoseEstimationMethod poseEstimationMethod = vpDetectorAprilTag::HOMOGRAPHY_VIRTUAL_VS;
    double tagSize = 0.053;
    float quad_decimate = 1.0;
    int nThreads = 6;
    double cam_px = 607.7191781; //px
    double cam_py = 612.0403312; //py
    double cam_u0 = 313.1891067; //u0
    double cam_v0 = 252.5071544; //v0
    double cam_kud = 0.02165764904; //kud
    double cam_kdu = -0.02156524548; //kdu
    bool cam_distortion = true;
    
    if(n.hasParam("pose_method")) {
        n.getParam("pose_method", par);
        poseEstimationMethod = (vpDetectorAprilTag::vpPoseEstimationMethod)atoi(par.c_str());
    }
    if(n.hasParam("tag_size")) {
        n.getParam("tag_size", par);
        tagSize = atof(par.c_str());
    }
    if(n.hasParam("camera_device")) {
        n.getParam("camera_device", par);
        opt_device = atoi(par.c_str());
    }
    if(n.hasParam("quad_decimate")) {
        n.getParam("quad_decimate", par);
        quad_decimate = (float)atof(par.c_str());
    }
    if(n.hasParam("nthreads")) {
        n.getParam("nthreads", par);
        nThreads = atoi(par.c_str());
    }
    if(n.hasParam("no_distortion")) {
        cam_distortion = false;
    }
    /********* TODO **************/
    // if(n.hasParam("camera_parameters")) {
    //     n.getParam("camera_parameters", par);
    //     try {
    //         if(cam_distortion) {
    //             std::stringstream ss(par);
    //             int i = 0;
    //             while(i < 6) {
                    
    //             }
    //         }
    //         else {
                
    //         }
    //     }
    //     catch(const std::exception &e) {
    //         ROS_ERROR("exception occured while parsing camera_parameters : %s", e.what());
    //     }
    // }
    /************** END(TODO) ***************/

    // store image
    vpImage<unsigned char> I;

    // up to about 30Hz on JIK's laptop ()
    ros::Rate rate(30.0);

    try {
        vpCameraParameters cam;
        if(cam_distortion) {
            cam.initPersProjWithDistortion(cam_px, cam_py, cam_u0, cam_v0, cam_kud, cam_kdu);
        }
        else {
            cam.initPersProjWithoutDistortion(cam_px, cam_py, cam_u0, cam_v0);
        }
        vpV4l2Grabber g;
        std::ostringstream device;
        device << "/dev/video" << opt_device;
        ROS_INFO("Use Video 4 Linux grabber on device %s", device.str().c_str());
        g.setDevice(device.str());
        g.setScale(1);
        g.open(I);

        std::ostringstream msg;
        msg << cam;
        ROS_INFO("Camera Parameters :\n%s", msg.str().c_str());
        msg.str(""); msg.clear();
        msg << poseEstimationMethod;
        ROS_INFO("Pose Estimation Method : %s", msg.str().c_str());
        msg.str(""); msg.clear();
        msg << tagFamily;
        ROS_INFO("Tag Family : %s", msg.str().c_str());
        ROS_INFO("using %d threads", nThreads);

        vpDisplay* d;
        d = new vpDisplayX(I);

        vpDetectorAprilTag detector(tagFamily);
        detector.setAprilTagQuadDecimate(quad_decimate);
        detector.setAprilTagPoseEstimationMethod(poseEstimationMethod);
        detector.setAprilTagNbThreads(nThreads);
        detector.setDisplayTag(false, vpColor::none, 2);
        detector.setZAlignedWithCameraAxis(0);

        tf2_ros::TransformBroadcaster br;
        geometry_msgs::TransformStamped transformStamped;
        
        while(ros::ok()) {
            ros::spinOnce();

            g.acquire(I);
            vpDisplay::display(I);
            std::vector<vpHomogeneousMatrix> cMo_vec;
            detector.detect(I, tagSize, cam, cMo_vec);
            for(std::vector<vpHomogeneousMatrix>::const_iterator i = cMo_vec.begin(); i != cMo_vec.end(); ++i) {
                vpDisplay::displayFrame(I, *i, cam, tagSize, vpColor::none, 5);
            }

            for(std::vector<vpHomogeneousMatrix>::const_iterator i = cMo_vec.begin(); i != cMo_vec.end(); ++i) {

                // Transform from vpHomogenousMatrix to ros_tf2
                transformStamped.header.frame_id = "camera";
                transformStamped.header.stamp = ros::Time::now();
                transformStamped.child_frame_id = "marker";
                const vpTranslationVector& translation = i->getTranslationVector();
                transformStamped.transform.translation.x = translation[0];
                transformStamped.transform.translation.y = translation[1];
                transformStamped.transform.translation.z = translation[2];
                const vpThetaUVector& rotation = i->getThetaUVector();
                const double theta = rotation.getTheta();
                const vpColVector u = rotation.getU();
                const double sinTheta2 = sin(0.5 * theta);
                transformStamped.transform.rotation.w = cos(0.5 * theta);
                transformStamped.transform.rotation.x = sinTheta2 * u[0];
                transformStamped.transform.rotation.y = sinTheta2 * u[1];
                transformStamped.transform.rotation.z = sinTheta2 * u[2];

                break;
            }
            br.sendTransform(transformStamped);

            vpDisplay::flush(I);

            rate.sleep();
        }
        
    }
    catch(const std::exception &e) {
        ROS_ERROR("Caught an exception : %s", e.what());
        return 1;
    }
    return 0;
    
}
